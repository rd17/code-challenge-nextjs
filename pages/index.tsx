import { NextPage } from 'next'
import Link from 'next/link'
import React, { useCallback, useState } from 'react'
import Layout from '../components/layout'
import useApiData from '../hooks/use-api-data'
import Airport from '../types/airport'
import { debounce } from '../utils/helpers'

const Page: NextPage = () => {
  const [text, setText] = useState('')
  const [query, setQuery] = useState('')

  const airports = useApiData<Airport[]>(`/api/airports?query=${query}`, [])

  const updateAirports = useCallback(
    debounce((value: string) => {
      setQuery(value)
    }, 0.7),
    []
  )

  const onEnterQuery = (evt: React.ChangeEvent<HTMLInputElement>) => {
    setText(evt.target.value)
    updateAirports(evt.target.value)
  }

  return (
    <Layout>
      <h1 className="text-2xl">Code Challenge: Airports</h1>

      <h2 className="mt-10 text-xl">Search Airports</h2>

      <div className="mt-2">
        <input
          className="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
          type="text"
          placeholder="Enter Name, IATA, City, or Country"
          value={text}
          onChange={onEnterQuery}
        />
      </div>

      <div>
        {airports.length > 0 ? (
          airports.map((airport) => (
            <Link
              href={`/airports/${airport.iata.toLowerCase()}`}
              key={airport.iata}
            >
              <a className="mt-5 flex items-center shadow p-5 border">
                <div>
                  {airport.name}, {airport.city}
                </div>
                <div className="ml-auto text-mono">{airport.country}</div>
              </a>
            </Link>
          ))
        ) : (
          <div className="mt-10 text-center">
            <h1 className="text-2xl">No Airports found</h1>
          </div>
        )}
      </div>
    </Layout>
  )
}

export default Page
