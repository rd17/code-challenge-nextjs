/**
 * Debounce a given function
 * @param fn - The function to debounce
 * @param delay - The delay for debouncing (in seconds)
 */
export const debounce = <Args extends any[], ReturnType extends any>(
  fn: (...args: Args) => ReturnType,
  delay: number
) => {
  let timeout: NodeJS.Timeout

  return (...args: Parameters<typeof fn>) => {
    timeout && clearTimeout(timeout)
    timeout = setTimeout(() => {
      fn(...args)
    }, delay * 1000)
  }
}
