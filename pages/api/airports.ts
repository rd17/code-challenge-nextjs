import { NextApiRequest, NextApiResponse } from 'next'

import { searchAirports } from '../../models/airport'

interface Request extends NextApiRequest {
  query: {
    query: string
  }
}

export default async (req: Request, res: NextApiResponse) => {
  try {
    const { query } = req.query
    const airports = await searchAirports(query)

    res.json(airports)
  } catch (err) {
    console.log('err:', err)
    res.json([])
  }
}
