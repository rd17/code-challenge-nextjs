import airports from '../data/airports.json'
import Airport from '../types/airport'

export const findAirportByIata = async (
  iata: string
): Promise<Airport | null> => {
  return airports.find((airport) => airport.iata === iata.toUpperCase()) || null
}

export const allAirports = async (): Promise<Airport[]> => {
  return airports
}

export const searchAirports = async (query: string): Promise<Airport[]> => {
  if (!query) return []

  return airports.filter(filterAirportByQuery(query.toLowerCase()))
}

const filterAirportByQuery = (query: string) => (airport: Airport) => {
  return (
    airport.name.toLowerCase().includes(query) ||
    airport.iata.toLowerCase().includes(query) ||
    airport.city.toLowerCase().includes(query) ||
    airport.country.toLowerCase().includes(query)
  )
}
